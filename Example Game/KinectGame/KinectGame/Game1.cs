using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using KinectXNA;
using Microsoft.Kinect;

namespace KinectGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        #region STUFF
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Vector2 screenDimensions;
        #endregion STUFF

        /// <summary>
        /// Kinect.
        /// </summary>
        private KinectManager kinect;

        /// <summary>
        /// Icon for the Kinect mic
        /// </summary>
        private Texture2D kinectMic;

        /// <summary>
        /// Position for Icon for the Kinect mic
        /// </summary>
        private Rectangle kinectMicPosition;

        /// <summary>
        /// Circle. To draw on screen.
        /// </summary>
        private Texture2D circle;

        /// <summary>
        /// A 1x1 white pixel
        /// </summary>
        private Texture2D pixel;

        /// <summary>
        /// Bools for enabling stuff on this tech demo
        /// </summary>
        private bool enableCamera = true, 
                     enableSkeleton = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 640;
            graphics.PreferredBackBufferHeight = 480;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //Initialise a thing that 
            screenDimensions = new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            //Initialize the Kinect
            kinect = new KinectManager(graphics.GraphicsDevice, new KinectSettings()
                {
                    PhrasesToCheck = new string[] { "show skeleton", "hide skeleton", "show camera", "hide camera", "enable preface", "disable preface", "close game" }
                });

            //Set the event method for recognised speech
            kinect.VoiceRecognition.SpeechRecognized += VoiceRecognition_SpeechRecognized;

            //Initialize XNA
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Load stuff
            kinectMic = Content.Load<Texture2D>(@"KinectMic");  //Kinect detected voice microphone
            kinectMicPosition = new Rectangle(graphics.PreferredBackBufferWidth - 75, graphics.PreferredBackBufferHeight - 75, 50, 50);
            circle = Content.Load<Texture2D>(@"Circle");        //Circle to draw on points on the kinect skeleton
            pixel = new Texture2D(graphics.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            pixel.SetData(new[] { Color.White });               //Pixel.

            //Initialize the DemoSkeleton class with textures
            DemoSkeleton.Initialize(circle, pixel, screenDimensions);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            //Start sprites!
            spriteBatch.Begin();
            
            //Draw the image from the camera on screen
            if (enableCamera)
                spriteBatch.Draw(kinect.VideoStream.GetCurrentVideoImage(),
                             kinect.VideoStream.GetCurrentVideoImage().Bounds, Color.White);

            //If the Kinect is ready to detect said words, put an icon on screen to show that
            if (kinect.VoiceRecognition.IsPrefaceSaid)
                spriteBatch.Draw(kinectMic, kinectMicPosition, Color.White);


            //If the Kinect doesn;t need a preface, then show something to (POC) represent that
            if (!kinect.VoiceRecognition.CheckForPreface)
                spriteBatch.Draw(kinectMic, kinectMicPosition, Color.DarkSlateBlue);

            //If the user wants, draw the skeleton
            if (enableSkeleton)
                DemoSkeleton.DrawSkeleton(spriteBatch, kinect);

            /**************************************************************************************/

            //if (kinect.Skeleton.FirstSkeleton != null)
            //{
            //    SkeletonPoint centerPosition = kinect.Skeleton.FirstSkeleton.Joints[JointType.ShoulderCenter].Position;
            //    SkeletonPoint handRight = kinect.Skeleton.FirstSkeleton.Joints[JointType.HandRight].Position;

            //    spriteBatch.Draw(circle, DemoSkeleton.CoonvertToScreenCoordinates(new Vector2(handRight.X - centerPosition.X, handRight.Y - centerPosition.Y)), null, Color.White, 0, new Vector2(circle.Width / 2, circle.Height / 2), 1, SpriteEffects.None, 0);
            //    //Console.WriteLine(new Vector2(handRight.X - centerPosition.X, handRight.Y - centerPosition.Y));
            //}

            /**************************************************************************************/

            spriteBatch.End();

            base.Draw(gameTime);
        }

        /**********************************************************************************************************/

        /// <summary>
        /// Method to run whenever speech is recognised
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VoiceRecognition_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            switch (e.Result)
            {
                case "SHOW SKELETON":
                    enableSkeleton = true;
                    break;
                case "HIDE SKELETON":
                    enableSkeleton = false;
                    break;
                case "SHOW CAMERA":
                    enableCamera = true;
                    break;
                case "HIDE CAMERA":
                    enableCamera = false;
                    break;
                case "ENABLE PREFACE":
                    kinect.VoiceRecognition.CheckForPreface = true;
                    break;
                case "DISABLE PREFACE":
                    kinect.VoiceRecognition.CheckForPreface = false;
                    break;
                case "CLOSE GAME":
                    base.Exit();
                    break;
            }
        }

        private void DrawLine(Vector2 point1, Vector2 point2, SpriteBatch spriteBatch)
        {
            //Translate the first and second point so that the second point is on the origin
            Vector2 toPoint = point2 - point1;

            //Find the angle between the vector above and the horizontal axis
            //http://www.allegro.cc/forums/thread/593438
            float angle = (float)Math.Atan2(toPoint.X, toPoint.Y);

            //Calculate the length of the line
            float length = toPoint.Length();

            //Draw the line on screen
            //spriteBatch.Draw(pixel, point1, null, Color.White, angle, Vector2.Zero, new Vector2(length, 2), SpriteEffects.None, 1);
            spriteBatch.Draw(pixel, new Rectangle((int)point1.X, (int)point1.Y, (int)length, 5), null, Color.White, angle, Vector2.Zero, SpriteEffects.None, 0);
        }
    }
}

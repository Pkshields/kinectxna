﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using KinectXNA;

namespace KinectGame
{
    public static class DemoSkeleton
    {
        /// <summary>
        /// 
        /// </summary>
        private static Dictionary<JointType, Vector2> jointLocations = new Dictionary<JointType, Vector2>();

        /// <summary>
        /// Circle. To draw on screen.
        /// </summary>
        private static Texture2D circle;

        /// <summary>
        /// A 1x1 white pixel
        /// </summary>
        private static Texture2D pixel;

        /// <summary>
        /// Dimensions of the screen
        /// </summary>
        private static Vector2 screenDimensions;

        /// <summary>
        /// Initialize the DemoSkeleton class
        /// </summary>
        /// <param name="circleTexture">Circle Texture</param>
        /// <param name="pixelTexture">Pixel texture</param>
        public static void Initialize(Texture2D circleTexture, Texture2D pixelTexture, Vector2 screenDimensionsVector)
        {
            circle = circleTexture;
            pixel = pixelTexture;
            screenDimensions = screenDimensionsVector;
        }

        /// <summary>
        /// Draw the demo skeleton on screen
        /// </summary>
        /// <param name="spriteBatch">Spritebatch</param>
        /// <param name="kinect">Kinect Manager</param>
        public static void DrawSkeleton(SpriteBatch spriteBatch, KinectManager kinect)
        {
            //Calculate the center of the circle so it can be drawn accurately on screen
            Vector2 circleOrigin = new Vector2(circle.Width / 2, circle.Height / 2);

            //As long as we have a skeleton to draw:
            if (kinect.Skeleton.FirstSkeleton != null)
            {
                //Calculate the position on screen of all the joints that Kinect tracks
                foreach (Joint joint in kinect.Skeleton.FirstSkeleton.Joints)
                {
                    //Using the KinectXNA SDK
                    jointLocations[joint.JointType] = CoonvertToScreenCoordinates(joint);
                }

                //Draw the center bones of the model
                DrawLine(jointLocations[JointType.HipCenter], jointLocations[JointType.Spine], spriteBatch);
                DrawLine(jointLocations[JointType.Spine], jointLocations[JointType.ShoulderCenter], spriteBatch);
                DrawLine(jointLocations[JointType.ShoulderCenter], jointLocations[JointType.Head], spriteBatch);

                //Draw the top left bones on the model
                DrawLine(jointLocations[JointType.ShoulderCenter], jointLocations[JointType.ShoulderLeft], spriteBatch);
                DrawLine(jointLocations[JointType.ShoulderLeft], jointLocations[JointType.ElbowLeft], spriteBatch);
                DrawLine(jointLocations[JointType.ElbowLeft], jointLocations[JointType.WristLeft], spriteBatch);
                DrawLine(jointLocations[JointType.WristLeft], jointLocations[JointType.HandLeft], spriteBatch);

                //Draw the top right bones on the model
                DrawLine(jointLocations[JointType.ShoulderCenter], jointLocations[JointType.ShoulderRight], spriteBatch);
                DrawLine(jointLocations[JointType.ShoulderRight], jointLocations[JointType.ElbowRight], spriteBatch);
                DrawLine(jointLocations[JointType.ElbowRight], jointLocations[JointType.WristRight], spriteBatch);
                DrawLine(jointLocations[JointType.WristRight], jointLocations[JointType.HandRight], spriteBatch);

                //Draw the bottom left bones on the model
                DrawLine(jointLocations[JointType.HipCenter], jointLocations[JointType.HipLeft], spriteBatch);
                DrawLine(jointLocations[JointType.HipLeft], jointLocations[JointType.KneeLeft], spriteBatch);
                DrawLine(jointLocations[JointType.KneeLeft], jointLocations[JointType.AnkleLeft], spriteBatch);
                DrawLine(jointLocations[JointType.AnkleLeft], jointLocations[JointType.FootLeft], spriteBatch);

                //Draw the bottom right bones on the model
                DrawLine(jointLocations[JointType.HipCenter], jointLocations[JointType.HipRight], spriteBatch);
                DrawLine(jointLocations[JointType.HipRight], jointLocations[JointType.KneeRight], spriteBatch);
                DrawLine(jointLocations[JointType.KneeRight], jointLocations[JointType.AnkleRight], spriteBatch);
                DrawLine(jointLocations[JointType.AnkleRight], jointLocations[JointType.FootRight], spriteBatch);

                //Draw a circle on all the joints on the model
                foreach (KeyValuePair<JointType, Vector2> joint in jointLocations)
                {
                    spriteBatch.Draw(circle, joint.Value, null, Color.White, 0, circleOrigin, 1, SpriteEffects.None, 0);
                }
            }
        }

        /// <summary>
        /// Draw a line on screen between two points
        /// </summary>
        /// <param name="point1">Point 1</param>
        /// <param name="point2">Point 2</param>
        /// <param name="spriteBatch">SpriteBatch</param>
        private static void DrawLine(Vector2 point1, Vector2 point2, SpriteBatch spriteBatch)
        {
            //Translate the first and second point so that the second point is on the origin
            Vector2 toPoint = point2 - point1;

            //Find the angle between the vector above and the horizontal axis
            //http://www.allegro.cc/forums/thread/593438
            float angle = (float)Math.Atan2(toPoint.Y, toPoint.X);

            //Calculate the length of the line
            float length = toPoint.Length();

            //Draw the line on screen
            //spriteBatch.Draw(pixel, point1, null, Color.White, angle, Vector2.Zero, new Vector2(length, 2), SpriteEffects.None, 1);
            spriteBatch.Draw(pixel, new Rectangle((int)point1.X, (int)point1.Y, (int)length, 3), null, Color.White, angle, Vector2.Zero, SpriteEffects.None, 0);
        }

        /// <summary>
        /// Convert the coordinates of each of the joints into screen coordinates
        /// </summary>
        /// <param name="joint">Joint to convert</param>
        /// <returns>Position of joint on screen</returns>
        public static Vector2 CoonvertToScreenCoordinates(Joint joint)
        {
            //Ensure that the user has filled in the screen dimensions
            if (screenDimensions != Vector2.Zero)
                //Convert the Joint position into a position on the screen
                return new Vector2(
                    (((0.5f * joint.Position.X) + 0.5f) * (screenDimensions.X)),
                    (((-0.5f * joint.Position.Y) + 0.5f) * (screenDimensions.Y)));

            else
                throw new ArgumentNullException("Screen Dimensions have not been set in the KinectManager settings");
        }

        /// <summary>
        /// Convert the coordinates of each of the joints into screen coordinates
        /// </summary>
        /// <param name="joint">Position to convert</param>
        /// <returns>Position of joint on screen</returns>
        public static Vector2 CoonvertToScreenCoordinates(Vector2 position)
        {
            //Ensure that the user has filled in the screen dimensions
            if (screenDimensions != Vector2.Zero)
                //Convert the Joint position into a position on the screen
                return new Vector2(
                    (((0.5f * position.X) + 0.5f) * (screenDimensions.X)),
                    (((-0.5f * position.Y) + 0.5f) * (screenDimensions.Y)));

            else
                throw new ArgumentNullException("Screen Dimensions have not been set in the KinectManager settings");
        }
    }
}

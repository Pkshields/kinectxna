﻿using System;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;

namespace KinectXNA
{
    /// <summary>
    /// Provides the settings for the Kinect sensor
    /// </summary>
    public class KinectSettings
    {
        /// <summary>
        /// Enable the video stream from the normal camera
        /// </summary>
        public bool EnableVideoStream = true;

        /// <summary>
        /// Enable the depth stream from the normal camera
        /// </summary>
        public bool EnableDepthStream = true;

        /// <summary>
        /// (If VideoStream enabled) Set the format of the video stream taken from the Kinect camera
        /// </summary>
        public KinectVideoStreamFormat VideoStreamImageFormat = KinectVideoStreamFormat.RgbResolution640x480Fps30;

        /// <summary>
        /// Enable the skeleton stream from the sensor
        /// </summary>
        public bool EnableSkeletonStream = true;

        /// <summary>
        /// Enable the voice recognition system
        /// </summary>
        public bool EnableVoiceRecognition = true;

        /// <summary>
        /// (If VoiceRecognition is enabled) List of phrases to check in the Voice Recognition system
        /// </summary>
        public string[] PhrasesToCheck = new string[] { };

        /// <summary>
        /// (If VoiceRecognition is enabled) Word said to preface voice recognition from the user
        /// </summary>
        public string PrefaceWord = "Kinect";

        /// <summary>
        /// (If VoiceRecognition is enabled) Set the confidence level where, if the detected word is less than, the sensor will not return then word as detected
        /// </summary>
        public float ConfidenceLevel = 0.4f;

        /// <summary>
        /// Initialize an instance of KinectSettings with the default settings
        /// </summary>
        public KinectSettings()
        { }
    }
}

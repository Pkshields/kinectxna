using System;
using Microsoft.Kinect;
using Microsoft.Xna.Framework.Graphics;

namespace KinectXNA
{
    /// <summary>
    /// Manager to managing the Kinect
    /// </summary>
    public class KinectManager
    {
                                                #region PROPERTIES

        /// <summary>
        /// Object holding the kinect sensor settings
        /// </summary>
        private KinectSensor kinect;

        /// <summary>
        /// Get the current status of the Kinect
        /// </summary>
        public KinectStatus Status
        {
            get { return kinect.Status; }
        }

        /// <summary>
        /// Set the elevation angle of the sensor. 
        /// Ensure that the value is between -27 and 27 as that is the outer limits of the Kinect motor.
        /// </summary>
        public int ElevationAngle
        {
            get { return kinect.ElevationAngle; }
            set
            {
                //Ensure that the value is between -27 and 27 as that is the outer limits of the Kinect motor.
                //IT WILL TRY TO GO MORE/LESS THOUGH and make horrible noises.
                if (value >= -27 || value <= 27)
                {
                    //Try to change the angle
                    try
                    {
                        kinect.ElevationAngle = value;
                    }
                    catch (Exception e)
                    {
                        throw new ArgumentException("Kinect API threw an error. Try not to change the Kinect tilt motor more than once per second.", e);
                    }
                }
                else
                    throw new ArgumentOutOfRangeException("Value must be between -27 and 27, or else your Kinect motor will try to break itself.");
            }
        }

        /// <summary>
        /// Access to the Video Stream from the Kinect
        /// </summary>
        public KinectVideoStream VideoStream
        {
            get; 
            private set; 
        }

        /// <summary>
        /// Access to the visual representation of the Depth Stream from the Kinect
        /// </summary>
        public KinectDepthStream DepthStream
        {
            get;
            private set;
        }

        /// <summary>
        /// Access to the Skeleton Stream from the Kinect
        /// </summary>
        public KinectSkeleton Skeleton
        {
            get;
            private set;
        }

        /// <summary>
        /// Access to the Voice Recognition system from the Kinect
        /// </summary>
        public KinectVoiceRecognition VoiceRecognition
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of the KinectManager, initializing the Kinect in the process
        /// </summary>
        /// <param name="graphics">Graphics device being used by XNA</param>
        public KinectManager(GraphicsDevice graphics) : this(graphics, new KinectSettings())
        { }

        /// <summary>
        /// Initialize an instance of the KinectManager, initializing the Kinect in the process
        /// </summary>
        /// <param name="graphics">Graphics device being used by XNA</param>
        /// <param name="settings">Settings used to configure the Kinect</param>
        public KinectManager(GraphicsDevice graphics, KinectSettings settings)
        {
            //Loop through all possibly connected Kinects
            foreach (KinectSensor sensor in KinectSensor.KinectSensors)
            {
                //If we find one that is connected, store it and close out of the loop. Job done!
                if (sensor.Status == KinectStatus.Connected)
                {
                    kinect = sensor;
                    break;
                }
            }

            //Ensure we didn't go out of the loop without finding a Kinect. If we did then throw an exception
            if (this.kinect == null)
            {
                throw new KinectNotFoundException("Connected Kinect Sensor is not found");
            }

            //If the user wants it enabled, enable the Video Stream
            if (settings.EnableVideoStream)
                VideoStream = new KinectVideoStream(kinect, graphics, settings.VideoStreamImageFormat);

            //If the user wants it enabled, enable the Depth Stream
            if (settings.EnableDepthStream)
                DepthStream = new KinectDepthStream(kinect, graphics);

            //If the user wants it enabled, enable the Skeleton Stream
            if (settings.EnableSkeletonStream)
                Skeleton = new KinectSkeleton(kinect);

            //Start the Kinect!
            kinect.Start();

            //If the user wants it enabled, enable the Voice Recognition engine
            if (settings.EnableVoiceRecognition)
                VoiceRecognition = new KinectVoiceRecognition(kinect, settings.PhrasesToCheck, settings.PrefaceWord, settings.ConfidenceLevel);
        }

                                                #endregion CONSTRUCTORS
    }
}

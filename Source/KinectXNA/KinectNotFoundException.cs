﻿using System;

namespace KinectXNA
{
    /// <summary>
    /// Exception representing when something is out of range of the Joystick
    /// </summary>
    public class KinectNotFoundException : Exception
    {
        /// <summary>
        /// Throw an exception for whenever the Kinect sensor is not found
        /// </summary>
        public KinectNotFoundException()
        { }

        /// <summary>
        /// Throw an exception for whenever the Kinect sensor is not found
        /// </summary>
        /// <param name="message">The message that describes the error</param>
        public KinectNotFoundException(string message)
            : base(message)
        { }

        /// <summary>
        /// Throw an exception for whenever the Kinect sensor is not found
        /// </summary>
        /// <param name="message">The message that describes the error</param>
        /// <param name="inner">The exception that is the cause for the current exception.</param>
        public KinectNotFoundException(string message, Exception inner)
            : base(message, inner)
        { }
    }
}
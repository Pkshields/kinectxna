﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using Microsoft.Xna.Framework.Graphics;

namespace KinectXNA
{
    /// <summary>
    /// Class containing all of the functions and variables needed to create the depth stream display
    /// </summary>
    public class KinectDepthStream
    {
        /// <summary>
        /// Graphics device which is used by XNA
        /// </summary>
        private GraphicsDevice graphics;

        /// <summary>
        /// Current depth image taken from the camera
        /// </summary>
        private Texture2D depthVideo;

        /// <summary>
        /// Object containing the currently selected kinect sensor
        /// </summary>
        private KinectSensor kinect;

        /// <summary>
        /// Array of shorts to hold the depth video frame data
        /// </summary>
        private short[] pixelData;

        /// <summary>
        /// Initialises a new instance of the kinect depth stream
        /// </summary>
        /// <param name="kinect">Takes in the current kinect sensor to be used by this manager</param>
        /// <param name="graphics">Takes in the graphics device used by this game</param>
        internal KinectDepthStream(KinectSensor kinect, GraphicsDevice graphics)
        {
            //Store the graphics device
            this.graphics = graphics;

            //Store the current kinect being used
            this.kinect = kinect;
            
            //Enable the depth stream, with the specified resolution and fps
            kinect.DepthStream.Enable(DepthImageFormat.Resolution320x240Fps30);
            
            //Initialises the current frame which we are taking in
            kinect.DepthFrameReady += KinectDepthFrameReady;
        }

        /// <summary>
        /// Called every time the kinect has a new image from the camera which we need to read in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KinectDepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            using (DepthImageFrame depthVideoFrame = e.OpenDepthImageFrame())
            {
                //Open the depth frame and check if it's null
                if (depthVideoFrame != null)
                {
                    //Initialises the pixelData array with the depth video frame information
                    pixelData = new short[depthVideoFrame.PixelDataLength];
                    
                    //Fills the array with data
                    depthVideoFrame.CopyPixelDataTo(pixelData);
                    
                    //Creates the actual texture2d for the depth video information
                    depthVideo = new Texture2D(graphics, depthVideoFrame.Width, depthVideoFrame.Height);
                    
                    //The normal colour stream returns a 32 bit image but the depth stream returns a 16 bit image so we need to convert this to 32 bit
                    depthVideo.SetData(ConvertDepthFrame(pixelData, kinect.DepthStream));
                }
            }
        }

        /// <summary>
        /// Returns the current depth frame for future use
        /// </summary>
        /// <returns></returns>
        public Texture2D GetCurrentDepthImage()
        {
            //Returns the current image from the camera
            return depthVideo;
        }

        /// <summary>
        /// Convert the detph data into XNA image data
        /// </summary>
        /// <param name="depthFrame"></param>
        /// <param name="depthStream"></param>
        /// <returns></returns>
        private byte[] ConvertDepthFrame(short[] depthFrame, DepthImageStream depthStream)
        {
            int RedIndex = 0, GreenIndex = 1, BlueIndex = 2, AlphaIndex = 3;

            byte[] depthFrame32 = new byte[depthStream.FrameWidth * depthStream.FrameHeight * 4];

            // We loop through the image and take a piece of information and convert it to a byte
            for (int i16 = 0, i32 = 0; i16 < depthFrame.Length && i32 < depthFrame32.Length; i16++, i32 += 4)
            {
                int player = depthFrame[i16] & DepthImageFrame.PlayerIndexBitmask;
                int realDepth = depthFrame[i16] >> DepthImageFrame.PlayerIndexBitmaskWidth;

                // Convert the depth information to an 8 bit intensity which we can display 
                byte intensity = (byte)(~(realDepth >> 4));

                // We then apply the intensity to each of the channels, red, green, then blue.
                depthFrame32[i32 + RedIndex] = (byte)(intensity);
                depthFrame32[i32 + GreenIndex] = (byte)(intensity);
                depthFrame32[i32 + BlueIndex] = (byte)(intensity);

                // And make the alpha channel opaque
                depthFrame32[i32 + AlphaIndex] = 255;
            }

            // We then return the byte array which contains the transformed pixel data
            return depthFrame32;
        }
    }
}

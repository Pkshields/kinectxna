﻿using System;
using System.Linq;
using System.IO;
using Microsoft.Speech.Recognition;
using Microsoft.Kinect;
using Microsoft.Speech.AudioFormat;
using MSSpeechRecognizedEventArgs = Microsoft.Speech.Recognition.SpeechRecognizedEventArgs;

namespace KinectXNA
{
    public class KinectVoiceRecognition
    {
                                                #region PROPERTIES

        /// <summary>
        /// The speech recognition engine
        /// </summary>
        private SpeechRecognitionEngine speechRecognizer;

        /// <summary>
        /// Bool check for if the preface word has been said before the phrase
        /// </summary>
        public bool IsPrefaceSaid;

        /// <summary>
        /// Check fi the preface word is detected before detecting a phrase
        /// </summary>
        public bool CheckForPreface;

        /// <summary>
        /// Set the confidence level where, if the detected word is less than, the sensor will not return then word as detected
        /// </summary>
        public float ConfidenceLevel;

        /// <summary>
        /// Word said to preface voice recognition from the user
        /// </summary>
        private string prefaceWord;

                                                #endregion PROPERTIES

                                                #region EVENTS

        /// <summary>
        /// Handler for event that fires whenever some speech has been recognised
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void SpeechRecognizedHandler(object sender, SpeechRecognizedEventArgs e);

        /// <summary>
        /// Event that fires whenever some speech has been recognised
        /// </summary>
        public event SpeechRecognizedHandler SpeechRecognized;

                                                #endregion EVENTS

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of the KinectVoiceRecognition class
        /// </summary>
        /// <param name="kinect">Kinect used by this manager</param>
        /// <param name="phrasesToCheck">Phrases for the Kinect to try and recognise</param>
        internal KinectVoiceRecognition(KinectSensor kinect, string[] phrasesToCheck, string prefaceWord, float confidenceLevel)
        {
            //Set the preface word, preface variable to false and the confidence level set by the user
            this.prefaceWord = prefaceWord.ToUpperInvariant();
            this.IsPrefaceSaid = false;
            this.ConfidenceLevel = confidenceLevel;
            this.CheckForPreface = true;

            //Create the speech recogniser
            speechRecognizer = CreateSpeechRecognizer(phrasesToCheck);

            //Start streaming audio from the Kinect

            //Set sensor audio source to variable
            KinectAudioSource audioSource = kinect.AudioSource;

            //Set the beam angle mode - the direction the audio beam is pointing
            //We want it to be set to adaptive
            audioSource.BeamAngleMode = BeamAngleMode.Adaptive;

            //Start the audioSource 
            Stream kinectStream = audioSource.Start();

            //Configure the incoming audio stream
            speechRecognizer.SetInputToAudioStream(
                kinectStream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));

            //Make sure the recognizer does not stop after completing... something     
            speechRecognizer.RecognizeAsync(RecognizeMode.Multiple);

            //Reduce background and ambient noise for better accuracy
            kinect.AudioSource.EchoCancellationMode = EchoCancellationMode.None;
            kinect.AudioSource.AutomaticGainControlEnabled = false;
        }

        /// <summary>
        /// This is where we create the speech recogniser
        /// </summary>
        /// <returns>The speech recogniser</returns>
        private SpeechRecognitionEngine CreateSpeechRecognizer(string[] phrasesToCheck)
        {
            //Set recognizer info from the Kinect sensor
            RecognizerInfo kinectInfo = GetKinectRecognizer();

            //Create instance of SpeechRecognitionEngine
            SpeechRecognitionEngine speechEngine;
            speechEngine = new SpeechRecognitionEngine(kinectInfo.Id);

            //Create the list of phrases we want to be recognised after the preface word has been said
            Choices choices = new Choices(prefaceWord);
            choices.Add(phrasesToCheck);

            //Set culture - language, country/region
            //Taken from the Kinect sensor during the recogniser info get
            GrammarBuilder gb = new GrammarBuilder { Culture = kinectInfo.Culture };
            gb.Append(choices);

            //Set up the grammar builder
            Grammar g = new Grammar(gb);
            speechEngine.LoadGrammar(g);

            //Set events for recognizing, hypothesising and rejecting speech
            speechEngine.SpeechRecognized += new EventHandler<MSSpeechRecognizedEventArgs>(speechEngine_SpeechRecognized);
            //sre.SpeechHypothesized += SreSpeechHypothesized;
            //sre.SpeechRecognitionRejected += SreSpeechRecognitionRejected;
            return speechEngine;
        }

        /// <summary>
        /// Get the speech recogniser info form the Kinect sensor. 
        /// Code taken from here: http://kin-educate.blogspot.co.uk/2012/06/speech-recognition-for-kinect-easy-way.html
        /// </summary>
        /// <returns>The speech recogniser</returns>
        private static RecognizerInfo GetKinectRecognizer()
        {
            Func<RecognizerInfo, bool> matchingFunc = r =>
            {
                string value;
                r.AdditionalInfo.TryGetValue("Kinect", out value);
                return "True".Equals(value, StringComparison.InvariantCultureIgnoreCase) && "en-US".Equals(r.Culture.Name, StringComparison.InvariantCultureIgnoreCase);
            };
            return SpeechRecognitionEngine.InstalledRecognizers().Where(matchingFunc).FirstOrDefault();
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Method to run whenever some sorta speech is recognised
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void speechEngine_SpeechRecognized(object sender, MSSpeechRecognizedEventArgs e)
        {
            //Very important! - change this value to adjust accuracy - the higher the value
            //the more accurate it will have to be, lower it if it is not recognizing you
            if (e.Result.Confidence < ConfidenceLevel)
            {
                //Send a null result to the user to say the word is a little jumbled, not recognised
                if (SpeechRecognized != null)
                    SpeechRecognized(this, new SpeechRecognizedEventArgs(null, e.Result.Confidence));
                return;
            }

            //Conver the result to uppercase so that is not a factor in checks
            string result = e.Result.Text.ToUpperInvariant();

            //If the preface word has been said, set the kinect check to true (if the user wants to check for preface word first)
            if (result == prefaceWord && CheckForPreface)
                IsPrefaceSaid = true;

            //If we are ready to check for a phrase
            if ((IsPrefaceSaid || !CheckForPreface) && result != prefaceWord)
            {
                //Send the phrase to the user, then reset the check
                if (SpeechRecognized != null)
                    SpeechRecognized(this, new SpeechRecognizedEventArgs(result, e.Result.Confidence));
                IsPrefaceSaid = false;
            }
        }

                                                #endregion METHODS
    }

    /// <summary>
    /// Event arguments passed whenever the KinectVoiceRecognition event is called
    /// </summary>
    public class SpeechRecognizedEventArgs : EventArgs
    {
        /// <summary>
        /// Words from your dictionary recognised by the speech recognition engine
        /// </summary>
        public string Result;

        /// <summary>
        /// Confidence of the sensor in the detected phrase
        /// </summary>
        public float Confidence;

        /// <summary>
        /// Initialize an instance of the KinectVoiceRecognition EventArgs
        /// </summary>
        /// <param name="result">Words from your dictionary recognised by the speech recognition engine</param>
        /// <param name="confidence">Confidence of the sensor in the detected phrase</param>
        internal SpeechRecognizedEventArgs(string result, float confidence)
        {
            Result = result;
            Confidence = confidence;
        }
    }
}

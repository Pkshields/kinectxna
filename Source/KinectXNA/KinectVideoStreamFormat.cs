﻿using System;
using Microsoft.Kinect;

namespace KinectXNA
{
    /// <summary>
    /// Allow for multiple different types of image format from the Kinect camera
    /// </summary>
    public enum KinectVideoStreamFormat
    {
        RgbResolution640x480Fps30 = ColorImageFormat.RgbResolution640x480Fps30,
        RgbResolution1280x960Fps12 = ColorImageFormat.RgbResolution1280x960Fps12
    };
}

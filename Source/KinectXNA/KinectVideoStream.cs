﻿using System;
using Microsoft.Kinect;
using Microsoft.Xna.Framework.Graphics;

namespace KinectXNA
{
    /// <summary>
    /// Class containing all the fuctions for the video camera on the Kinect sensor
    /// </summary>
    public class KinectVideoStream
    {
                                                #region PROPERTIES

        /// <summary>
        /// Graphics device being used by XNA - needed for generating the image
        /// </summary>
        private GraphicsDevice graphics;

        /// <summary>
        /// Reusable array for getting the image bytes from the camera for provessing
        /// </summary>
        private Byte[] pixelData;

        /// <summary>
        /// Reusable array got converting the RGB image into the BGR inage used by Kinect
        /// </summary>
        private Byte[] bgraPixelData;

        /// <summary>
        /// Current image taken from the camera
        /// </summary>
        private Texture2D colorVideo;

                                                #endregion PROPERTIES

                                                #region EVENTS

        /// <summary>
        /// Handler for event that fires whenever a new video frame is ready and processed from the camera
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void VideoFrameReadyHandler(object sender, KinectVideoStreamEventArgs e);

        /// <summary>
        /// Event that fires whenever a new video frame is ready and processed from the camera
        /// </summary>
        public event VideoFrameReadyHandler VideoFrameReady;

                                                #endregion EVENTS

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of the KinectVideoStream class
        /// </summary>
        /// <param name="kinect">Kinect used by this manager</param>
        /// <param name="graphics">Graphics device used by this game</param>
        /// <param name="format">Format of the image to get from the camera</param>
        internal KinectVideoStream(KinectSensor kinect, GraphicsDevice graphics, KinectVideoStreamFormat format)
        {
            //Store the graphics device
            this.graphics = graphics;

            //Enable the RGB video stream with the required settings
            kinect.ColorStream.Enable((ColorImageFormat) format);

            //Event to call a method every time a color frame is ready from the camera
            kinect.ColorFrameReady += new EventHandler<ColorImageFrameReadyEventArgs>(Kinect_ColorFrameReady);
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Get the current image from the camera
        /// </summary>
        /// <returns>Returns the current image from the camera</returns>
        public Texture2D GetCurrentVideoImage()
        {
            //Returns the current image from the camera
            return colorVideo;
        }

        /// <summary>
        /// Called whenever the Kinect has another image from the camera to store
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Kinect_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            //Gets the most recent frame of color data from the camera
            using (ColorImageFrame colorVideoFrame = e.OpenColorImageFrame())
            {
                //If there is data from the camera
                //"When the project is still initialising the Kinect Sensor is already sending data but it may not contain anything"
                if (colorVideoFrame != null)
                {
                    //If the arrays haven't been initializes yet, initialize them
                    if (pixelData == null)
                    {
                        //Create array for the pixel data
                        pixelData = new Byte[colorVideoFrame.PixelDataLength];

                        //Create the array for converted BGRA pixel data
                        bgraPixelData = new Byte[colorVideoFrame.PixelDataLength];
                    }

                    //Copy the pixel data from the image frame into thus array of Bytes
                    colorVideoFrame.CopyPixelDataTo(pixelData);

                    //As the Kinect uses RGBA and XNA uses BGRA (for whatrever reason) we mute convert the pixel data from RGBA to BGRA
                    for (int i = 0; i < pixelData.Length; i += 4)
                    {
                        bgraPixelData[i] = pixelData[i + 2];        //Move R
                        bgraPixelData[i + 1] = pixelData[i + 1];    //Move G (to the same place)
                        bgraPixelData[i + 2] = pixelData[i];        //Move A
                        bgraPixelData[i + 3] = 255;                 //The video comes with 0 alpha so it is transparent, remove transparency
                    }

                    //Dispose of the old video texture
                    if (colorVideo != null)
                        colorVideo.Dispose();

                    //Create the new video texture
                    colorVideo = new Texture2D(graphics, colorVideoFrame.Width, colorVideoFrame.Height);

                    //Pass our Pixel Data from the camera to the Texture2D variable
                    colorVideo.SetData(bgraPixelData);

                    //If the VideoFrameReady event is set by the user, call it and send the new Texture2D
                    if (VideoFrameReady != null)
                        VideoFrameReady(this, new KinectVideoStreamEventArgs(colorVideo));
                }
            }
        }

                                                #endregion METHODS
    }

    /// <summary>
    /// Event arguments passed whenever the KinectVideoStream event is called
    /// </summary>
    public class KinectVideoStreamEventArgs : EventArgs
    {
        /// <summary>
        /// Current image from the camera stream
        /// </summary>
        public Texture2D VideoStream;

        /// <summary>
        /// Initialize an instance of the KinectVideoStream EventArgs
        /// </summary>
        /// <param name="videoStream">Current image from the camera stream</param>
        internal KinectVideoStreamEventArgs(Texture2D videoStream)
        {
            this.VideoStream = videoStream;
        }
    }
}
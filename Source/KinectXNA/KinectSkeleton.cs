﻿using System;
using System.Linq;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;

namespace KinectXNA
{
    /// <summary>
    /// Class containing all the functions necessary to manage the Skeleton imformation taken from Kinect
    /// </summary>
    public class KinectSkeleton
    {
                                                #region PROPERTIES

        /// <summary>
        /// All the current skeleton data for the skeletons
        /// </summary>
        public Skeleton[] AllSkeletons
        {
            get;
            private set;
        }

        /// <summary>
        /// The first tracked skeleton data
        /// </summary>
        public Skeleton FirstSkeleton
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region EVENTS

        /// <summary>
        /// Handler for event that fires whenever a new skeleton data is ready
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void SkeletonReadyHandler(object sender, KinectSkeletonEventArgs e);

        /// <summary>
        /// Event that fires whenever a new skeleton data is ready
        /// </summary>
        public event SkeletonReadyHandler SkeletonReady;

                                                #endregion EVENTS

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of the KinectSkeleton class
        /// </summary>
        /// <param name="kinect">Kinect used by this manager</param>
        internal KinectSkeleton(KinectSensor kinect)
        {
            //Enable skeleton tracking stream
            kinect.SkeletonStream.Enable();

            //Add event handler for whenever the skeleton stream is ready up update
            kinect.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(kinect_SkeletonFrameReady);
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Called whenever the Kinect has updated positions of the skeletons on screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void kinect_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            //Using the skeleton frame sent by the Kinect
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                //Ensure it's not null (the delay upon startup or if it is disabled or if there is no skeletons being tracked)
                if (skeletonFrame != null)
                {
                    //If the number of skeletons has changed since last update then reinitialise the array
                    if ((AllSkeletons == null) || (AllSkeletons.Length != skeletonFrame.SkeletonArrayLength))
                    {
                        AllSkeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    }

                    //Copy the skeleton data to our array
                    skeletonFrame.CopySkeletonDataTo(AllSkeletons);

                    //Get the first player that is found in the array to be tracked << is this the closest player?
                    FirstSkeleton = (from s in AllSkeletons where s.TrackingState == SkeletonTrackingState.Tracked select s)
                                .FirstOrDefault();

                    //If the SkeletonReady event is set by the user, call it and send the new skeleton datas (yup)
                    if (SkeletonReady != null)
                        SkeletonReady(this, new KinectSkeletonEventArgs(AllSkeletons, FirstSkeleton));
                }
            }
        }

                                                #endregion METHODS
    }

    /// <summary>
    /// Event arguments passed whenever the KinectSkeleton event is called
    /// </summary>
    public class KinectSkeletonEventArgs : EventArgs
    {
        /// <summary>
        /// All the current skeleton data for the skeletons
        /// </summary>
        public Skeleton[] AllSkeletons;

        /// <summary>
        /// The first tracked skeleton data
        /// </summary>
        public Skeleton FirstSkeleton;

        /// <summary>
        /// Initialize an instance of the KinectVideoStream EventArgs
        /// </summary>
        internal KinectSkeletonEventArgs(Skeleton[] allSkeletons, Skeleton firstSkeleton)
        {
            AllSkeletons = allSkeletons;
            FirstSkeleton = firstSkeleton;
        }
    }
}

# KinectXNA
 
KinectXNA is an XNA library, built to simplify many of the generic methods used to access, control and calculate the Kinect sensor.

KinectXNA is licensed under the New BSD License.